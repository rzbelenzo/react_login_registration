import React, {Component} from 'react';
import {BrowserRouter,Switch,Route} from 'react-router-dom';
// import axios from 'axios';
import Home_Page from './components/home_page'
import Login_Page from './components/login_page'
import Registration_Page from './components/registration'

class App extends Component{

  constructor(){
    super();
    this.state = {
      loggedInStatus : "NOT_LOGGED_IN",
      user : {}
    }

    this.handleLogin = this.handleLogin.bind(this)
  }

  // checkLoginStatus(){
  //   axios.get(
  //     'http://localhost:3000/',
  //     {withCredentials:true}
  //   )
  //   .then((res)=>{
  //     console.log(res);
  //   })
  //   .catch((err)=>{
  //     console.log(err);
  //   })
  // }

  // componentDidMount(){
  //   this.checkLoginStatus();
  // }

  handleLogin(data){
    // update the log status
    this.setState({
      loggedInStatus : data.status,
      user : data.user
    })
  }

  render(){
    return(
      <>
        <div className="app">
          <BrowserRouter>
            <Switch>

              <Route 
                exact path={"/"} 
                render={props => (
                  <Login_Page {...props} handleLogin={this.handleLogin} loggedInStatus={this.state.loggedInStatus} />
                )}
              ></Route>  

              <Route 
                exact path={"/home_page"} 
                render={props => (
                  <Home_Page {...props} loggedInStatus={this.state.loggedInStatus} user_credentials={this.state.user} />
                )}
              ></Route>  

              <Route 
                exact path={"/registration_page"} 
                render={props => (
                  <Registration_Page {...props} />
                )}
              ></Route>  

            </Switch>          
          </BrowserRouter>
        </div>
      </>
    )
  }
}

export default App;

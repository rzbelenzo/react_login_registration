import React from 'react';
import axios from 'axios';
import Popoutmodal from './popout_modal'
import '../App.css';
import '../css/registration_page.css';

class Registration extends React.Component{

  constructor(props){
    super(props);
    this.state = {
        firstname : '',
        lastname : '',
        address : '',
        email : '',
        username : '',
        password : '',
        
        modal : {
            show : false,
            indicator : '',
            message : ''
        }
    }
  }

    // input change event -------------------------
    _handleChange = ({target}) => {
        const {name,value} = target;
        this.setState({ [name]:value })
        console.log('change state here')
    }

  _revert_login_page = () =>{
    // re-direct route to Login page / starting page
     this.props.history.push('/');     
  }

  _resetUserInputs = () =>{
    this.setState({
        firstname : '',
        lastname : '',
        address : '',
        email : '',
        username : '',
        password : '',
     })
    }

  _trigger_register_account = (event) =>{
    event.preventDefault();
    const{firstname,lastname,address,email,username,password} = this.state;

    const payload = {
        credentials : {
            username : username,
            password : password
        },
        information : {
            firstname : firstname,
            lastname : lastname,
            address : address,
            email : email
        }
    };

    this.setState({
        modal : {
            show : true,
            indicator : 'Processing_Modal',
            message : 'Registering Account, please wait.'
        }
    })
    axios({
        url:'/api/register_account',
        method : 'POST',
        data: payload
    })
    .then((res) => {
        this._resetUserInputs();
        this.setState({
            modal : {
                show : true,
                indicator : 'Success_Modal',
                message : 'Account Successfully Registered'
            }
        })
        setTimeout(() => {
            this.setState({
                modal : {
                    show : false,
                    indicator : '',
                    message : ''
                }
            })
        }, 2000);
        // this._revert_login_page();
    })
    .catch((err) => {
        this.setState({
            modal : {
                show : true,
                indicator : 'Failed_Modal',
                message : `Error : ${err}`
            }
        })
        setTimeout(() => {
            this.setState({
                modal : {
                    show : false,
                    indicator : '',
                    message : ''
                }
            })
        }, 2000);
    });;
  }

  render(){
    return(
      <>
         <div className="reg_body"></div>
		 <div className="reg_grad"></div>
	     <br></br>
         <div className="reg" >
                <div className="reg_header">
                    <div>Register<span>Account</span></div>
                </div>
            <div className="register_form">
                <div className="container">  
               
                    <form className="contact" action="" method="post" onSubmit={this._trigger_register_account} >
                        <fieldset>
                            <input placeholder="firstname" name="firstname" type="text" tabIndex="1"  onChange={this._handleChange} value={this.state.firstname}  required autoFocus></input>
                        </fieldset>
                        <fieldset>
                            <input placeholder="Lastname" name="lastname" type="text" tabIndex="2"  onChange={this._handleChange} value={this.state.lastname} required autoFocus></input>
                        </fieldset>
                        <fieldset>
                            <input placeholder="Address" name="address" type="text" tabIndex="3"  onChange={this._handleChange} value={this.state.address} required autoFocus></input>
                        </fieldset>
                        <fieldset>
                            <input placeholder="Email" name="email" type="email" tabIndex="4"  onChange={this._handleChange} value={this.state.email} required></input>
                        </fieldset>
                    
                        <span className="reg_txt2" >
                               Account 
                        </span>
                        
                        <div className="max-width float-left">
                            <div className="half-width float-left small-padding-right">
                                <fieldset>
                                    <input placeholder="username"  name="username" type="text" tabIndex="5" onChange={this._handleChange} value={this.state.username} required></input>
                              </fieldset>
                            </div>

                            <div className="half-width display-inline-block">
                                <fieldset>
                                    <input placeholder="password" name="password" type="password" tabIndex="6"  onChange={this._handleChange} value={this.state.password} required></input>
                              </fieldset>
                            </div>
                        </div>
                        
                        <fieldset>
                            <button name="submit" type="submit" >Register</button>
                            <span className="reg_txt2" >
                               Already have an account? <a className="login_a" onClick={this._revert_login_page} > Login here </a>
                            </span>
                        </fieldset>
                    </form>
                    </div>
                </div>
         </div>

         <div>
            <Popoutmodal show={this.state.modal.show} indicator={this.state.modal.indicator} message={this.state.modal.message} ></Popoutmodal>
         </div>
      </>
    )
  }
}

export default Registration;

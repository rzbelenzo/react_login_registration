import React, {createRef} from 'react';
import logo from '../logo.svg';
import axios from 'axios';
import Popoutmodal from './popout_modal'
import '../css/login_page.css';
import '../App.css';

class Login_Page extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      login_credentials : { 
        username : '',
        password : ''
      },
      modal : {
        show : false,
        indicator : '',
        message : ''
      },
    }

    this.inputRef = createRef()
  }

  componentDidMount = () =>{
    const user = JSON.parse(localStorage.getItem("user"));
    console.log(user)
    if (user !== null) {
      // if(user.isAuthenticated === true){
      //   console.log('throug here')
        this.props.history.push('/home_page');     
      // }
    }
  }

  // input change event -------------------------
 
  _username_handleChange = ({target}) =>{
    const {value} = target;
    this.setState({
      login_credentials : {
        username:value,
        password : this.state.login_credentials.password
      }
    })
  }

  _password_handleChange = ({target}) =>{
    const {value} = target;
    this.setState({
      login_credentials : {
        username: this.state.login_credentials.username,
        password : value
      }
    })
  }

  _handleKeyPress = (event) =>{
    if(event.key === 'Enter'){
     this._trigger_login();
    }
  }

  handleSuccessfulLogin(data){
    // TODO updat parent component
    this.props.handleLogin(data);

    // set LocalStorage
    localStorage.setItem("user",JSON.stringify({name : data.user.credentials, isAutehnticated : true}));

    // re-direct route to home page after success login
     this.props.history.push('/home_page');     
  }

  _formValidation = () => {
    const {username,password} = this.state.login_credentials;
    return username !== "" && password !== "" ? true : false;
  }

  _trigger_login = () =>{
    console.log('login here')

    if(this._formValidation()){
      
    const payload = {
      username : this.state.login_credentials.username,
      password : this.state.login_credentials.password
    };

    this.setState({
      modal : {
          show : true,
          indicator : 'Processing_Modal',
          message : 'Please Wait.'
      }
    })

     // -------------------------
     axios({
        url:'/api/fetch_account',
        method : 'POST',
        data: payload
      })
      .then((res) => {
        console.log(res.data)
          if(res.data.status === "LOGGED_IN"){
              this.handleSuccessfulLogin(res.data);
          }else{
            this.setState({
              modal : {
                  show : true,
                  indicator : 'Failed_Modal',
                  message : 'Account Not Found'
              }
            })
              setTimeout(() => {
                this.setState({
                    modal : {
                        show : false,
                        indicator : '',
                        message : ''
                    }
                })
             }, 2000);
          }
      })
      .catch((err) => {
        this.setState({
          modal : {
              show : true,
              indicator : 'Failed_Modal',
              message : `Error : ${err}`
          }
        })
        setTimeout(() => {
            this.setState({
                modal : {
                    show : false,
                    indicator : '',
                    message : ''
                }
            })
        }, 2000);
      });;
    }else{
      this.setState({
        modal : {
            show : true,
            indicator : 'Processing_Modal',
            message : `Provide Username and password`
        }
      })
      setTimeout(() => {
          this.setState({
              modal : {
                  show : false,
                  indicator : '',
                  message : ''
              }
          })
      }, 2000);
    }

  }

  _trigger_register = ()=>{
    // re-direct route to registration page after success login
     this.props.history.push('/registration_page');     
  }

  render(){
    return(
      <>
       <div className="body"></div>
		   <div className="grad"></div>
       <div className="header">
        <div>App<span>Login</span></div>
       </div>
	     <br></br>
       <img src={logo} className="App-logo" alt="logo" /> 
       <div className="login" >
          <input type="text" placeholder="Username" name="username" ref={this.inputRef} value={this.state.login_credentials.username} onChange={this._username_handleChange} ></input><br></br>
          <input type="password" placeholder="Password" name="password" value={this.state.login_credentials.password} onChange={this._password_handleChange} ></input><br></br>
          <input type="button"  value="Login" onClick={this._trigger_login}></input>
          <div className="register_button" >
                <span className="txt2" >
                            Don't have an account? <a className="register_a" onClick={this._trigger_register}> Register here </a>
                </span>
          </div>
        </div>

        <div>
            <Popoutmodal show={this.state.modal.show} indicator={this.state.modal.indicator} message={this.state.modal.message} ></Popoutmodal>
         </div>
      </>
    )
  }
}

export default Login_Page;

import React from 'react';
import '../css/font-awesome.css';
import '../css/popout_modal.css';

export default class Popout_Modal extends React.Component {
    render() {
      const {show,indicator,message} = this.props;
      if(!show){
        return null;
      }

      let toast_class = "";
      // let toast_icon = "";

      switch(indicator){
        case "Failed_Modal":
          toast_class = "toast_Failed show animate_zoom";
          // toast_icon = "fa fa-warning .font-regular-xl";
          break;
        case "Success_Modal":
          toast_class = "toast_Success show animate_zoom";
          // toast_icon = "fa fa-info .font-regular-xl";
          break;
        case "Warning_Modal":
            toast_class = "toast_Warning show animate_zoom";
            // toast_icon = "fa fa-info .font-regular-xl";
            break;
        case "Processing_Modal":
            toast_class = "toast_Processing show animate_zoom";
            // toast_icon = "fa fa-info .font-regular-xl";
            break;
      }

      return(
        <>
          <div className={toast_class}>
            {/* <div className="float-left">
                <span className={toast_icon}></span>
            </div> */}
            <span className="no-margin modal_message">{message}</span>
          </div>
        </>
      )
    }
}
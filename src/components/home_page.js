import React from 'react';
import '../css/home_page.css';

class Home_Page extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      user_credentials : {}
    }
  }

  componentDidMount = () =>{
    const user = JSON.parse(localStorage.getItem("user"));
    // this.setState({
    //   user_credentials : user
    // })

    console.log(user)
    if (user === null) {
      this.props.history.push('/');     
    }
  }

  _logoutbtn = () =>{
    localStorage.clear();
    this.props.history.push('/');
  }

  render(){
    return(
      <>
        <div className="home_page_body"></div>
        <div className="home_page_grad"></div>
        <br></br> 
         <div className="home_page" >
                <div className="home_page_header">
                    <div>Welcome to <span>MainPage</span> </div>
                </div>

                <div className="home_page_container">
                   <div className="container">
                   <span className="page_text" >Status : <span>TRUE</span>  </span>
                   <br></br>
                   {/* <span className="page_text" >Firstname : <span>{this.state.user_credentials.information.firstname}</span>  </span>
                   <br></br>
                   <span className="page_text" >Lastname : <span>{this.state.user_credentials.information.lastname}</span>  </span>
                   <br></br>
                   <span className="page_text" >Address : <span>{this.state.user_credentials.information.address}</span>  </span>
                   <br></br>
                   <span className="page_text" >Email : <span>{this.state.user_credentials.information.email}</span>  </span> */}
                   <br></br>
                   <button className="logout_btn" name="submit" type="submit" onClick={this._logoutbtn} >Logout</button>
                   </div>
                </div>
         </div>
      </>
    )
  }
}

export default Home_Page;

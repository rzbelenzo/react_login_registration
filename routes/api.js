const express = require('express')
const bcrypt = require('bcrypt')

const router = express.Router();

const AccountList = require('../models/accounts')

// POST - send data to the server
// GET - get data from the server

// ROUTES
router.post('/fetch_account',(req,res)=>{
     AccountList.findOne({
            'credentials.username':req.body.username,
        })
        .then(async(data)=>{
              if(data != null){
                  try{
                  const comp = await bcrypt.compare(req.body.password,data.credentials.password);
                if(comp){
                    const return_data = {
                        status : 'LOGGED_IN',
                        user : data
                    }
                    res.json(return_data)
                }else{
                    const return_data = {
                        status : 'NOT_FOUND',
                          user : {}
                     }
                    res.json(return_data)             
                }
              }catch(err){
                res.json("ERROR")          
              }
            }else{
                const return_data = {
                    status : 'NOT_FOUND',
                      user : {}
                 }
                res.json(return_data)              
             }
        })
         .catch((err)=>{
            console.log(err);
        })
})

// SAVE AREA ------------------------------

router.post('/register_account',async(req,res)=>{
    try{
        const salt = await bcrypt.genSalt();
        const hashedPassword = await bcrypt.hash(req.body.credentials.password,salt);

        req.body.credentials.password = hashedPassword;

        const data = req.body;
        AccountList.create(data)
        .then((data)=>{
            res.json({msg : 'data Saved'})
        })
        .catch((err)=>{
            console.log(err)
            res.json({msg : `Error : ${err}`})
        })
    }catch(err){
        console.log(err);
        res.status(500).json({msg:'Account not registered'})
    }
    
})

module.exports = router;

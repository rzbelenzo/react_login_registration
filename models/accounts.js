const mongoose = require('mongoose');

//Schema
const Schema = mongoose.Schema;
const AccountsSchema = new Schema({
    // flag : Boolean,
    // position : String,
    credentials : {
        username : String,
        password : String,
    },
    information : {
        firstname : String,
        lastname : String,
        address : String,
        email : String,
    }
});

// Model
const Accounts = mongoose.model('react_db_account',AccountsSchema);

module.exports = Accounts;

const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
const PORT = process.env.PORT || 8080;

const routes = require('./routes/api');
const Local = "mongodb://localhost:27017/electron_db"
const Url_Cluster_Atlas = "mongodb+srv://Lorenzo:8462017935@cluster0-jmaf3.mongodb.net/electron_db?retryWrites=true&w=majority";

mongoose.connect(Url_Cluster_Atlas,{useNewUrlParser: true, useUnifiedTopology: true})

mongoose.connection.on('connected',()=>{
    console.log('Status : Connected to mongodb - ');
})

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// app.use(cors())
app.use(cors({origin: true, credentials: true}));

// HTTP request logger
app.use(morgan('tiny'));
app.use('/api',routes);

app.listen(PORT,console.log(`Server : ${PORT}`))